function slider(sliderParent) {
    var slider = sliderParent.querySelector('.slider__items');
    var sliderItems = sliderParent.querySelectorAll(".slider__item");
    var sliderContainer = sliderParent.querySelector(".slider__container");
    var resizeTimeout;
    var resizeStart = false;
    var translateX = 0;
    function setSliderWidth() {
        resizeStart = false;
        let slidersWidth = sliderItems[0].offsetWidth * sliderItems.length;
        slider.style['width'] = `${slidersWidth}px`;
    }
    window.addEventListener('resize',  function () {
        //To toggle between vertical scrolling and flex wrap, this code only runs one time at the resize start
        //and resize end to decrease dom manipulation
        if (!resizeStart) {
            slider.style.width = null;
            slider.style.transform = null;
            translateX = 0;
            resizeStart = true;
        }
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(setSliderWidth, 500);
    });
    //To enable vertical scrolling
    setSliderWidth();

    //next and prev functionality
    sliderParent.querySelector(".slider__next").addEventListener('click', function (e) {
        translateX += -(sliderContainer.offsetWidth);
        slider.style.transform = 'translateX(' + (translateX) + 'px)';
    });
    sliderParent.querySelector(".slider__prev").addEventListener('click', function (e) {
        translateX += (sliderContainer.offsetWidth);
        slider.style.transform = 'translateX(' + (translateX) + 'px)';
    });
};
slider(document.getElementsByClassName('slider')[0])